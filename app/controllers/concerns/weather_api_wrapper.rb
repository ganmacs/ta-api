require 'net/http'
require 'json'

module WeatherApiWrapper
  PATH = 'http://api.openweathermap.org/data/2.5/weather?q=Tokyo,jp'.freeze

  def today_weather
    main = weather['main']
    {
      Date: Time.zone.now.to_s,
      MinTemp: f_to_c(main['temp_min']),
      MaxTemp: f_to_c(main['temp_max']),
      calender: { name: '国立競技場落成記念日',  description: '１９５８（昭和３３）年のこの日、神宮外苑に完成した国立霞ヶ丘陸上競技場の落成式が行われた。１９６４（昭和３９）年には、東京オリンピックの開閉会式場になった。'},
      MainWeather: weather['weather'].first['icon']
    }.to_json
  end

  private

  def f_to_c(f)
    format('%#.03g', f.to_f - 273.15)
  end

  def weather
    @weather ||= weather_json
  end

  def weather_json
    url = URI.parse(PATH)
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|http.request(req) }
    JSON.parse(res.body)
  end
end
