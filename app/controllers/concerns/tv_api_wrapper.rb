require 'net/http'
require 'json'
require 'pp'

module TvApiWrapper
  PATH = 'http://api.nhk.or.jp/v1/pg/now/130/g1.json?key=x2PUfKzZFhynNF49q8a9kYIpPsudGGzy'

  def now_program
    pp present = tv['nowonair_list']['g1']['present']
    present['title']
    {
      startTime: present['start_time'],
      endTime: present['end_time'],
      channel: present['service']['name'],
      title: present['title']
    }
  end

  private

  def tv
    @tv ||= tv_json
  end

  def tv_json
    url = URI.parse(PATH)
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|http.request(req) }
    JSON.parse(res.body)
  end
end
