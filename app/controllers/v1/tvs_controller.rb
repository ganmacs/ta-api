class V1::TvsController < V1::ApplicationController
  include TvApiWrapper

  def index
    render json: now_program
  end
end
