class V1::WeathersController < V1::ApplicationController
  include WeatherApiWrapper

  def index
    render json: today_weather
  end
end
